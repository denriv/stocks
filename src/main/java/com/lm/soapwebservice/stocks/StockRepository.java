package com.lm.soapwebservice.stocks;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import com.lm.stocks.services.Stock;

@Component
public class StockRepository {
	
    private static final Map<String, Stock> stocks = new HashMap<>();
    
    @PostConstruct
    public void initData() {
        // initialize stocks map
    }
 
    public Stock findStock(String name) {
        return stocks.get(name);
    }

}
