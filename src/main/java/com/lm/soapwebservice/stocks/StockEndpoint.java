package com.lm.soapwebservice.stocks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.lm.stocks.services.GetStock;
import com.lm.stocks.services.GetStockResponse;

@Endpoint
public class StockEndpoint {

	private static final String NAMESPACE_URI = "http://services.stocks.lm.com";

	private StockRepository stockRepository;

	@Autowired
    public StockEndpoint(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getStock")
	@ResponsePayload
	public GetStockResponse getStock(@RequestPayload GetStock request) {
		GetStockResponse response = new GetStockResponse();
		response.setGetStockReturn(stockRepository.findStock(request.getSymbol()));

		return response;
	}

}
