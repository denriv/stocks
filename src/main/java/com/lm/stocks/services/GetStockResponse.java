//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.01.21 at 09:20:46 AM PST 
//


package com.lm.stocks.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getStockReturn" type="{http://services.stocks.lm.com}Stock"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getStockReturn"
})
@XmlRootElement(name = "getStockResponse")
public class GetStockResponse {

    @XmlElement(required = true)
    protected Stock getStockReturn;

    /**
     * Gets the value of the getStockReturn property.
     * 
     * @return
     *     possible object is
     *     {@link Stock }
     *     
     */
    public Stock getGetStockReturn() {
        return getStockReturn;
    }

    /**
     * Sets the value of the getStockReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Stock }
     *     
     */
    public void setGetStockReturn(Stock value) {
        this.getStockReturn = value;
    }

}
